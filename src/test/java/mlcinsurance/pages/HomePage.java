package mlcinsurance.pages;

import net.thucydides.core.annotations.Step;

public class HomePage {
	
	MLCInsuranceHomePage home;

	@Step ("This step will verify the title")
	public void verifyTitle()
	{
		home.verifyTitle();
	}
	
	@Step ("This step will search the desired insurance type by name")
	public void searchInsuranceTypeClick() 
	{
		home.searchInsuranceTypeClick();
	}
	
}

