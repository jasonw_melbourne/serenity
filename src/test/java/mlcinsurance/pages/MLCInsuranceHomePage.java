package mlcinsurance.pages;

import org.junit.Assert;

import net.serenitybdd.core.annotations.findby.By;

import net.thucydides.core.pages.PageObject;

public class MLCInsuranceHomePage extends PageObject{
	
	public void verifyTitle()
	{
		String title=getDriver().getTitle();
		Assert.assertTrue(title.contains("MLC Life"));
	}
	
	public void searchInsuranceTypeClick() 
	{
		getDriver().findElement(By.xpath("/html/body/div[1]/div/header/div/nav[2]/button")).click();

		$(By.id("q")).typeAndEnter("LifeView");

		getDriver().findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div/main/section/ol/li[1]/div/a/div/h2")).click();
	}
	

}
