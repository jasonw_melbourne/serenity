package mlcinsurance.pages;

import net.thucydides.core.annotations.Step;

public class LifeViewPage {

	MLCLifeViewPage lifeview;
	
	@Step ("This step will verify the title of LifeView page")
	public void verifyTitle()
	{
		lifeview.verifyTitle();
	}
	
	@Step ("This step will verify that bread curmbs are correct")
	public void verifyBreadcrumb() 
	{
		lifeview.verifyBreadcrumb();
	}
	
	@Step ("Click the Request a Demo button/link")
	public void clickRequestDemo()
	{
		lifeview.clickRequestaDemo();
	}
	
	@Step ("Fill the Request Demo form")
	public void fillRequestaDemoForm (String name, String company, String email, String phone)
	{
		lifeview.fillRequestaDemoForm(name, company, email, phone);
	}
	
	@Step ("Submit the request")
	public void clickBookaDemoButton ()
	{
		
	}
}


