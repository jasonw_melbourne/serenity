package mlcinsurance.pages;

import org.junit.Assert;

import net.serenitybdd.core.annotations.findby.By;

import net.thucydides.core.pages.PageObject;

public class MLCLifeViewPage extends PageObject{

	private static final String BREADCRUMBTEXT="Home Partnering with us Superannuation funds LifeView";
	
	public void verifyTitle()
	{
		String title=getDriver().getTitle();
		Assert.assertTrue(title.contains("LifeView"));
	}
	
	public void clickRequestaDemo() 
	{
		//this is to click the Request a Demo element
		getDriver().findElement(By.xpath("/html/body/div[1]/div/div[3]/div[2]/div/main/div[2]/p[2]/a/span")).click();

	}
	
	
	public void verifyBreadcrumb ()
	{
		//the bread crumb
		String breadcrumb=getDriver().findElement(By.xpath("/html/body/div[1]/div/div[3]/div[1]/div/ul")).getText();
		Assert.assertTrue(breadcrumb.equals(BREADCRUMBTEXT));
	}

	public void fillRequestaDemoForm(String name, String company, String email, String phone)
	{
		//Name
		$(By.id("fxb_06355718-98ed-434b-8db4-317b0d7a8d19_Fields_8b6cea70-1386-487c-811d-6f0ca7308e00__Value")).type(name);
		
		//Company
		$(By.id("fxb_06355718-98ed-434b-8db4-317b0d7a8d19_Fields_6ba7f412-8c65-48cc-a6a6-5e89845f83aa__Value")).type(company);
		
		//Email
		$(By.id("fxb_06355718-98ed-434b-8db4-317b0d7a8d19_Fields_5d676ad7-7797-4b94-9201-6fcf95a3409b__Value")).type(email);
		
		//Phone
		$(By.id("fxb_06355718-98ed-434b-8db4-317b0d7a8d19_Fields_a999ffc5-eaaf-477f-ab59-20bfc112d0f5__Value")).type(phone);
		
	}
}
