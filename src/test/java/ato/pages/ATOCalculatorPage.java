package ato.pages;


import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import net.serenitybdd.core.annotations.findby.By;

import net.thucydides.core.pages.PageObject;

public class ATOCalculatorPage extends PageObject{
	
	public void selectIncomeYear(String financialYear)
	{
		WebElement select = getDriver().findElement(By.xpath("/html/body/form/div[5]/div/div/div[1]/div/div/div/ul/li[3]/div/div/div/div/div/div/div/div[2]/div[2]/div/div/div/div[4]/div[1]/div/div/div[2]/div/div[1]/div[1]/div/div/div/div/div/div/div/select"));
		Select dropDown = new Select(select); 
		dropDown.selectByVisibleText(financialYear);
	}
	

	public void inputIncome(String income) 
	{
		
		//Phone
		//$(By.id("fxb_06355718-98ed-434b-8db4-317b0d7a8d19_Fields_a999ffc5-eaaf-477f-ab59-20bfc112d0f5__Value")).type(phone);
		$(By.id("texttaxIncomeAmt")).type(income);
	}

	public void selectResidencyStatus(String residentStatus) 

	{
		
		String residentforfullyear="/html/body/form/div[5]/div/div/div[1]/div/div/div/ul/li[3]/div/div/div/div/div/div/div/div[2]/div[2]/div/div/div/div[4]/div[3]/div/div/div[1]/div/div[1]/div[1]/div/div/div/div[1]/fieldset/div[3]/div/div[1]/label/span[2]";
		String nonresidentfullyear="/html/body/form/div[5]/div/div/div[1]/div/div/div/ul/li[3]/div/div/div/div/div/div/div/div[2]/div[2]/div/div/div/div[4]/div[3]/div/div/div[1]/div/div[1]/div[1]/div/div/div/div[1]/fieldset/div[3]/div/div[2]/label/span[2]";
		String partyearresident="/html/body/form/div[5]/div/div/div[1]/div/div/div/ul/li[3]/div/div/div/div/div/div/div/div[2]/div[2]/div/div/div/div[4]/div[3]/div/div/div[1]/div/div[1]/div[1]/div/div/div/div[1]/fieldset/div[3]/div/div[3]/label/span[2]";
		String res="";
		if (residentStatus.equals("Resident for full year"))
		{
			res=residentforfullyear;
			getDriver().findElement(By.xpath(res)).click();
		}
		
		if (residentStatus.equals("Non-resident for full year"))
		{
			res=nonresidentfullyear;
			getDriver().findElement(By.xpath(res)).click();
		}
		
		if (residentStatus.equals("Part-year resident"))
		{
			res=partyearresident;
			getDriver().findElement(By.xpath(res)).click();
		}
	
		
		
		if (residentStatus.equals("Part-year resident"))
		{
			WebElement select = getDriver().findElement(By.name("ddl-residentPartYearMonths"));
			Select dropDown = new Select(select); 
			dropDown.selectByValue("6");			
		}
	}

	public void clickSubmit()
	{
		
		String submit="/html/body/form/div[5]/div/div/div[1]/div/div/div/ul/li[3]/div/div/div/div/div/div/div/div[2]/div[2]/div/div/div/div[5]/div/div/button[2]";
		//same Submit button with different text
	
		$(By.xpath(submit)).click();
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void verifyTax(String tax)
	{
		String payable="/html/body/form/div[5]/div/div/div[1]/div/div/div/ul/li[3]/div/div/div/div/div/div/div/div[2]/div[2]/div/div[1]/div/div/div/p";
		String amount=$(By.xpath(payable)).getText();
		System.out.println("get tax:" +amount);
		System.out.println("expected tax:" +tax);
		
		Assert.assertTrue(amount.contains(tax));
	}
	public void clickRestart()
	{
		String restart="/html/body/form/div[5]/div/div/div[1]/div/div/div/ul/li[3]/div/div/div/div/div/div/div/div[2]/div[2]/div/div[5]/div/div/button[2]";
		//same Submit button with different text
		$(By.xpath(restart)).click();
	}
	
	public void searchInsuranceTypeClick() 
	{
		getDriver().findElement(By.xpath("/html/body/div[1]/div/header/div/nav[2]/button")).click();

		$(By.id("q")).typeAndEnter("LifeView");

		getDriver().findElement(By.xpath("/html/body/div[1]/div/div[3]/div/div/main/section/ol/li[1]/div/a/div/h2")).click();
	}
	
	public void calculateTax(String financialYear, String income, String residentialStatus)
	{
		selectIncomeYear(financialYear);
		inputIncome(income);
		selectResidencyStatus(residentialStatus);
	}
}