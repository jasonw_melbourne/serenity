package ato.pages;

import net.thucydides.core.annotations.Step;

public class CalculatorPage {
	
	ATOCalculatorPage calc;

	@Step ("This step will select the income year")
	public void selectIncomeYear(String financialYear)
	{
		calc.selectIncomeYear(financialYear);
	}
	
	@Step ("This step will input the income")
	public void inputIncome(String income) 
	{
		calc.inputIncome(income);
	}
	@Step ("This step will select the residency status")
	public void selectResidencyStatus(String residentStatus) 
	{
		calc.selectResidencyStatus(residentStatus);
	}
	
	@Step ("This step will submit the information for calculation")
	public void clickSubmit()
	{
		calc.clickSubmit();
	}
	
	
	@Step ("This step will verify the tax amount")
	public void verifyTax(String tax)
	{
		calc.verifyTax(tax);
	}
	
	@Step ("This step will clear the info and restart the calculato")
	public void clickRestart()
	{
		calc.clickRestart();
	}
	
	@Step ("This is merged steps to calculate tax based on Financial Year, Income, and Resident status")
	public void calculateTax (String financialYear, String income, String residentStatus)
	{
		calc.calculateTax(financialYear, income, residentStatus);
	}
}

