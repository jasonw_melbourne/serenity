package testcases;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import mlcinsurance.pages.HomePage;
import mlcinsurance.pages.LifeViewPage;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;

@RunWith(SerenityRunner.class)
public class SearchLifeViewRequestDemoTest {

	@Managed
	WebDriver driver;
	
	@Steps
	HomePage home;
	
	@Steps
	LifeViewPage lifeview;
	
	@Test
	public void connectMLCInsurance()
	{
		driver.get("https://www.mlcinsurance.com.au/");
		driver.manage().window().maximize();
		home.verifyTitle();
		home.searchInsuranceTypeClick();
		
		lifeview.verifyTitle();
		lifeview.verifyBreadcrumb();
		lifeview.clickRequestDemo();
		lifeview.fillRequestaDemoForm("Jason", "Life-is-Great", "Hope@testdemo-task.org", "0912356789");
		
		//No screenshots taken, keep it for 3 seconds
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
