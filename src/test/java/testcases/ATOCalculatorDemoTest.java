package testcases;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import ato.pages.CalculatorPage;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;

@RunWith(SerenityRunner.class)
public class ATOCalculatorDemoTest {

	@Managed
	WebDriver driver;
		
	@Steps
	CalculatorPage calc;
	
	@Test
	public void ATOIncomeCalculation()
	{
		driver.get("https://www.ato.gov.au/Calculators-and-tools/Host/?anchor=STC&anchor=STC#STC/questions");
		driver.manage().window().maximize();
		
		verifyATOFinancialYearIncomeTax("2019-20", "80000", "Resident for full year","17,547.00" );
		verifyATOFinancialYearIncomeTax("2018-19", "120000", "Non-resident for full year","40,350.00");
		verifyATOFinancialYearIncomeTax("2017-18", "160000", "Part-year resident","47,056.96" );
	}
	
	public void safeWait(int milliseconds) 
	{
		
		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
	}
	
	public void verifyATOFinancialYearIncomeTax(String financialYear, String income, String residential, String tax)
	{
		calc.calculateTax(financialYear, income, residential);
		Actions action = new Actions(driver);
		action.sendKeys(Keys.PAGE_DOWN).build().perform();
		safeWait(3000);
		calc.clickSubmit();
		calc.verifyTax(tax);
		action.sendKeys(Keys.PAGE_DOWN).build().perform();
		safeWait(3000);
		calc.clickRestart();
	}
	
	
}
