# MLC Insurance

This is the simplest possible build script setup for Serenity BDD using Java. 

It uses MLC Insurance client portal to request a demo as an example.

It also uses ATO tax calculator as the second test case.

## Major Features

1. Page Object Model is implemented as part of the project.
2. Serenity with JUnit and other plug-ins are used.
3. 2 scenarios are executed in parallel.

## Test Scenarios

1. In MLC Insurance home page, search LifeView and request a demo.
2. In ATO calculator page, calculate the tax payable based on financial year, income and residential status.

## To Run the Test

You need Edge Version 91.0.864.41 (Official build) (64-bit)

Download the pack
Open a command window and run this command in the folder:

    mvn clean verify


